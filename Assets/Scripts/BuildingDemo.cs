﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDemo : MonoBehaviour
{
    public BuildingSettings settings;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            settings.buildingSize.x = Random.Range(1, 14);
            settings.buildingSize.y = Random.Range(1, 14);
            Building b = BuildingGenerator.Generate(settings);
            GetComponent<BuildingRenderer>().Render(b);
        }
    }
    
}
