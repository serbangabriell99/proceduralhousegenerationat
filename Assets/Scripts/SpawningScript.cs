﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningScript : MonoBehaviour
{

   // public GameObject building;
    public Vector3 center;
    public Vector3 size;

    void Start()
    {
       // SpawnBuilding();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SpawnBuilding();
        }
    }

    public void SpawnBuilding()
    {
        Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), 
            Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));

        this.gameObject.transform.position = pos;
      //  Instantiate(building, pos, Quaternion.identity);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}
