﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultRoofStrategy : RoofStartegy
{
    public override Roof GenerateRoof(BuildingSettings settings, RectInt bounds )
    {
        return new Roof();
    }
}
